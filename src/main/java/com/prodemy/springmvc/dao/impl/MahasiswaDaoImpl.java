/**
 * 
 */
package com.prodemy.springmvc.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.prodemy.springmvc.dao.MahasiswaDao;
import com.prodemy.springmvc.model.Mahasiswa;

/**
 * @author wyant
 *
 */
@Repository
public class MahasiswaDaoImpl implements MahasiswaDao {
	private Connection con = null;
	private String url = "jdbc:postgresql://localhost/form?stringtype=unspecified";
	private String username = "postgres";
	private String password = "Baba3003";

	public MahasiswaDaoImpl() {
		try {
			Class.forName("org.postgresql.Driver");			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Connection getConnection() throws Exception {
		if (con!=null) {
			if (con.isClosed()) {
				con = DriverManager.getConnection(url, username, password);
			}
		} else {
			con = DriverManager.getConnection(url, username, password);
		}
		return con;
	}
	
	@Override
	public Mahasiswa findByNim(String nim) throws Exception {
		Mahasiswa result = null;

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			StringBuilder query = new StringBuilder("select * from \"Mahasiswa\" where \"NIM\"=?");
			ps = getConnection().prepareStatement(query.toString());
			ps.setString(1, nim);
			rs = ps.executeQuery();
			if (rs.next()) {
				result = new Mahasiswa();
				result.setNim(rs.getString("NIM"));
				result.setNama(rs.getString("Nama"));
				result.setAlamat(rs.getString("Alamat"));
				result.setTanggalLahir(rs.getString("Tanggal Lahir"));
				result.setProdi(rs.getString("Prodi"));
				result.setFakultas(rs.getString("Fakultas"));
			}
		} finally {
			try {
				rs.close();
			} catch (Exception ignored) {}
			try {
				ps.close();
			} catch (Exception ignored) {}
		}
		
		return result;
	}

	@Override
	public void deleteByNim(String nim) throws Exception {
		PreparedStatement ps = null;
		
		try {
			StringBuilder query = new StringBuilder("delete from \"Mahasiswa\" where \"NIM\"=?");
			ps = getConnection().prepareStatement(query.toString());
			ps.setString(1, nim);
			ps.executeUpdate();
		} finally {
			try {
				ps.close();
			} catch (Exception ignored) {}			
		}
	}

	@Override
	public void insert(Mahasiswa mhs) throws Exception {
		PreparedStatement ps = null;
		
		try {
			StringBuilder query = new StringBuilder("insert into \"Mahasiswa\" values (?,?,?,?,?,?)");
			ps = getConnection().prepareStatement(query.toString());
			ps.setString(1, mhs.getNim());
			ps.setString(2, mhs.getNama());
			ps.setString(3, mhs.getAlamat());
			ps.setString(4, mhs.getTanggalLahir());
			ps.setString(5, mhs.getProdi());
			ps.setString(6, mhs.getFakultas());
			ps.executeUpdate();
		} finally {
			try {
				ps.close();
			} catch (Exception ignored) {}			
		}
	}

	@Override
	public void update(Mahasiswa mhs) throws Exception {
		PreparedStatement ps = null;
		
		try {
			StringBuilder query = new StringBuilder("update \"Mahasiswa\" set \"Nama\"=?, \"Alamat\"=?, \"Tanggal Lahir\"=?, \"Prodi\"=?, \"Fakultas\"=? where \"NIM\"=?");
			ps = getConnection().prepareStatement(query.toString());
			ps.setString(6, mhs.getNim());
			ps.setString(1, mhs.getNama());
			ps.setString(2, mhs.getAlamat());
			ps.setString(3, mhs.getTanggalLahir());
			ps.setString(4, mhs.getProdi());
			ps.setString(5, mhs.getFakultas());
			ps.executeUpdate();
		} finally {
			try {
				ps.close();
			} catch (Exception ignored) {}			
		}
	}

	@Override
	public List<Mahasiswa> findAll() throws Exception {
		List<Mahasiswa> result = null;

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			StringBuilder query = new StringBuilder("select * from \"Mahasiswa\"");
			ps = getConnection().prepareStatement(query.toString());
			rs = ps.executeQuery();
			if (rs.next()) {
				result = new ArrayList<>();
				do {
					Mahasiswa mhs = new Mahasiswa();
					mhs.setNim(rs.getString("NIM"));
					mhs.setNama(rs.getString("Nama"));
					mhs.setAlamat(rs.getString("Alamat"));
					mhs.setTanggalLahir(rs.getString("Tanggal Lahir"));
					mhs.setProdi(rs.getString("Prodi"));
					mhs.setFakultas(rs.getString("Fakultas"));
					
					result.add(mhs);
				} while (rs.next());
			}
		} finally {
			try {
				rs.close();
			} catch (Exception ignored) {}
			try {
				ps.close();
			} catch (Exception ignored) {}
		}
		
		return result;
	}

}
